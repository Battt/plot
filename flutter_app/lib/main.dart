import 'package:bezier_chart/bezier_chart.dart';
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      debugShowCheckedModeBanner: false,
      home: MyHomePage(),
    );
  }
}
class MyHomePage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    final fromDate = DateTime(2019, 3, 22);
    final toDate = DateTime.now();

    final date1 = DateTime.now().subtract(Duration(days: 2));
    final date2 = DateTime.now().subtract(Duration(days: 3));

    final date3 = DateTime.now().subtract(Duration(days: 35));
    final date4 = DateTime.now().subtract(Duration(days: 36));

    final date5 = DateTime.now().subtract(Duration(days: 65));
    final date6 = DateTime.now().subtract(Duration(days: 64));

    return Row(
      children: <Widget>[
        Expanded(
          flex: 1,
          child: Container(
            color: Colors.white70,
          ),
        ),
        Expanded(
          flex: 9,
          child: Center(
           child: Container(
            color: Colors.white70,
            height: MediaQuery.of(context).size.height / 2,
            width: MediaQuery.of(context).size.width,
             child: BezierChart(
              bezierChartScale: BezierChartScale.MONTHLY,
              fromDate: fromDate,
              toDate: toDate,
              selectedDate: toDate,
              series: [
                BezierLine(
                  lineColor: Color.fromARGB(0, 222, 185, 1),
                  onMissingValue: (dateTime) {
                  if (dateTime.month.isEven) {
                   return 8.0;
                  }
                   return 5.0;
                  },
                  data: [
                    DataPoint<DateTime>(value: 4, xAxis: date1),
                    DataPoint<DateTime>(value: 6, xAxis: date2),
                    DataPoint<DateTime>(value: 5, xAxis: date3),
                    DataPoint<DateTime>(value: 8, xAxis: date4),
                    DataPoint<DateTime>(value: 9, xAxis: date5),
                    DataPoint<DateTime>(value: 7, xAxis: date6),
                  ],
                ),
                ],
                config: BezierChartConfig(
                  verticalIndicatorStrokeWidth: 3.0,
                  verticalIndicatorColor: Colors.black26,
                  showVerticalIndicator: true,
                  verticalIndicatorFixedPosition: false,
                  backgroundGradient: LinearGradient(
                    colors: [
                      Color.fromRGBO(0, 222, 185, 0.2),
                      Color.fromRGBO(0, 222, 185, 0),
                    ],
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                  ),
                  footerHeight: 30.0,
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }
}